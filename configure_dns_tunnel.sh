#!/usr/bin/bash

systemctl stop iodine-server

DOMAIN=''
until echo -n ${DOMAIN} | grep -P "^(?!:\/\/)([a-zA-Z0-9-_]+\.)*[a-zA-Z0-9][a-zA-Z0-9-_]+\.[a-zA-Z]{2,11}?$" > /dev/null ; do
        read -p 'Enter the domain name to use: ' DOMAIN
done

PASSWORD=''
until echo -n ${PASSWORD} | grep -vP "^$" > /dev/null ; do
        read -p 'Enter the shared secret password: (32 characters max) ' PASSWORD
done

IP=''
until echo -n ${IP} | grep -E "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)" > /dev/null ; do
        read -p 'Enter the tunnel IP: (ex: 192.168.39.1) ' IP
done

MASK=''
until echo -n ${MASK} | grep -P "[89]|[12][0-9]|3[0-2]" > /dev/null; do
        read -p 'Enter the CIDR mask: (ex: 27) ' MASK
done

cat > /etc/sysconfig/iodine-server<< EOF
OPTIONS="-D -c -P ${PASSWORD} ${IP}/${MASK} ${DOMAIN}"
EOF

systemctl start iodine-server